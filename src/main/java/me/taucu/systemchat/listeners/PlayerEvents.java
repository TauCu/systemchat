package me.taucu.systemchat.listeners;

import me.taucu.systemchat.SystemChat;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class PlayerEvents implements Listener {

    @EventHandler
    public void onLogin(PlayerLoginEvent e) {
        SystemChat.attachHandler(e.getPlayer(), e.getAddress());
    }

}
