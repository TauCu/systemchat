package me.taucu.systemchat;

import me.taucu.systemchat.commands.SystemChatCommand;
import me.taucu.systemchat.listeners.PlayerEvents;
import me.taucu.systemchat.net.DuplexHandlerImpl;
import me.taucu.systemchat.util.SecureProfileDisabler;
import me.taucu.systemchat.util.TextFilterCrippler;
import org.bstats.bukkit.Metrics;
import org.bstats.charts.SimplePie;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.net.InetAddress;
import java.util.logging.Level;

public class SystemChat extends JavaPlugin {

    public static SystemChat instance;

    public boolean configSpoofSecureServer = true;
    public boolean logNetworkAttachmentExceptions = true;

    private Metrics metrics;

    public static SystemChat getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;

        getCommand("systemchat:systemchat").setExecutor(new SystemChatCommand());

        reloadConfig();

        if (SecureProfileDisabler.disableEnforceSecureProfile()) {
            getLogger().warning("Your server had enforceSecureProfile enabled. Option has been disabled.");
        }
        TextFilterCrippler.crippleTextFilter();

        Bukkit.getPluginManager().registerEvents(new PlayerEvents(), this);
        Bukkit.getOnlinePlayers().forEach(SystemChat::attachHandler);

        try {
            this.metrics = new Metrics(this, 17825);
            metrics.addCustomChart(new SimplePie("enforce_secure_profile", () -> String.valueOf(Bukkit.isEnforcingSecureProfiles())));
        } catch (Throwable t) {
            getLogger().log(Level.WARNING, "Error while enabling bStats", t);
        }
    }

    @Override
    public void onDisable() {
        Bukkit.getOnlinePlayers().stream()
                .filter(p -> !p.hasMetadata("NPC"))
                .forEach(p -> DuplexHandlerImpl.detach(p, DuplexHandlerImpl.NAME));
    }

    @Override
    public void reloadConfig() {
        saveDefaultConfig();
        super.reloadConfig();
        if (getConfig().getDouble("config version") != getConfig().getDefaults().getDouble("config version")) {
            getLogger().warning("Config version mismatch. Consider regenerating config.");
        }
        configSpoofSecureServer = getConfig().getBoolean("spoof secure server");
        logNetworkAttachmentExceptions = getConfig().getBoolean("log network attachment exceptions");
    }

    public static void attachHandler(Player player) {
        attachHandler(player, player.getAddress().getAddress());
    }

    public static void attachHandler(Player player, InetAddress address) {
        try {
            if (!player.hasMetadata("NPC")) {
                new DuplexHandlerImpl(player)
                        .attach(address);
            }
        } catch (Exception ex) {
            SystemChat inst = SystemChat.getInstance();
            if (inst.logNetworkAttachmentExceptions) {
                inst.getLogger().log(Level.SEVERE, "Failed to attach network handler to \"" + player.getName() + "\"", ex);
            } else {
                inst.getLogger().warning("Failed to attach network handler to \"" + player.getName() + "\"");
            }
        }
    }

}
