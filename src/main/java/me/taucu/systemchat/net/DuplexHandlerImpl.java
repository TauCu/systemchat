package me.taucu.systemchat.net;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import me.taucu.systemchat.SystemChat;
import me.taucu.systemchat.util.DuplexHandler;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.LastSeenMessages;
import net.minecraft.network.chat.RemoteChatSession;
import net.minecraft.network.protocol.game.*;
import org.bukkit.entity.Player;

import java.util.BitSet;
import java.util.UUID;

public class DuplexHandlerImpl extends DuplexHandler {

    public static final String NAME = "me.taucu.systemchat:handler";

    public final SystemChat instance = SystemChat.getInstance();
    public final UUID playerUUID;

    public DuplexHandlerImpl(Player player) {
        super(NAME);
        this.playerUUID = player.getUniqueId();
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (msg instanceof ServerboundChatPacket packet) { // unsign inbound messages
            msg = new ServerboundChatPacket(packet.message(), packet.timeStamp(), 0, null, new LastSeenMessages.Update(0, new BitSet(0)));
        } else if (msg instanceof ServerboundChatCommandSignedPacket packet) { // unsign inbound commands
            msg = new ServerboundChatCommandPacket(packet.command());
        } else if (msg instanceof ServerboundChatSessionUpdatePacket packet) { // invalidate profile key
            msg = new ServerboundChatSessionUpdatePacket(new RemoteChatSession.Data(packet.chatSession().sessionId(), null));
        }

        super.channelRead(ctx, msg);
    }

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        if (msg instanceof ClientboundPlayerChatPacket packet) { // convert outbound chat messages
            var comp = packet.unsignedContent() == null ? Component.literal(packet.body().content()) : packet.unsignedContent();

            comp = packet.chatType().decorate(comp);

            msg = new ClientboundSystemChatPacket(comp, false);
        } else if (msg instanceof ClientboundDeleteChatPacket) { // prevent chat delete as all messages are system messages
            return;
        } else if (msg instanceof ClientboundLoginPacket packet && instance.configSpoofSecureServer) { // spoof server secure profile
            msg = new ClientboundLoginPacket(packet.playerId(),
                    packet.hardcore(),
                    packet.levels(),
                    packet.maxPlayers(),
                    packet.chunkRadius(),
                    packet.simulationDistance(),
                    packet.reducedDebugInfo(),
                    packet.showDeathScreen(),
                    packet.doLimitedCrafting(),
                    packet.commonPlayerSpawnInfo(),
                    true);
        }

        super.write(ctx, msg, promise);
    }

}
