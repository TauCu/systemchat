package me.taucu.systemchat.util;

import net.minecraft.server.MinecraftServer;
import net.minecraft.server.dedicated.DedicatedServer;
import net.minecraft.server.dedicated.DedicatedServerProperties;

public class SecureProfileDisabler {

    public static boolean disableEnforceSecureProfile() {
        DedicatedServer server = (DedicatedServer) MinecraftServer.getServer();
        if (!server.getProperties().enforceSecureProfile
                && server.getProperties().properties.getProperty("enforce-secure-profile").equalsIgnoreCase("false")) return false;
        server.settings.update(properties -> {
            properties.properties.setProperty("enforce-secure-profile", "false");
            properties = new DedicatedServerProperties(properties.properties, server.options);
            return properties;
        });
        return true;
    }

}
