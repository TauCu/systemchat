package me.taucu.systemchat.util;

import net.minecraft.server.MinecraftServer;
import net.minecraft.server.dedicated.DedicatedServer;
import net.minecraft.server.network.ServerTextFilter;

import java.lang.reflect.Field;

public class TextFilterCrippler {

    public static void crippleTextFilter() {
        try {
            DedicatedServer server = (DedicatedServer) MinecraftServer.getServer();
            for (Field f : DedicatedServer.class.getDeclaredFields()) {
                if (ServerTextFilter.class.isAssignableFrom(f.getType())) {
                    f.setAccessible(true);
                    f.set(server, null);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
