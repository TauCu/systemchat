package me.taucu.systemchat.commands;

import me.taucu.systemchat.SystemChat;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.configuration.InvalidConfigurationException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.logging.Level;

public class SystemChatCommand implements TabExecutor {

    public static final List<String> BASE_COMPLETIONS = List.of("about", "reload");

    @SuppressWarnings("deprecation")
    public final String aboutMessage = ChatColor.YELLOW + "SystemChat " + ChatColor.GRAY + "version " + ChatColor.YELLOW + SystemChat.getInstance().getDescription().getVersion()
            + ChatColor.GRAY + " by " + ChatColor.YELLOW + "TauCubed"
            + "\n" + ChatColor.GRAY + "Blocks chat reporting, popups, warnings and more."
            + "\n" + ChatColor.GRAY + "Usage: " + ChatColor.YELLOW + "/systemchat reload";

    @SuppressWarnings("deprecation")
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (args.length > 0) {
            switch (args[0].toLowerCase()) {
                case "reload":
                    if (sender.hasPermission("systemchat.command.reload")) {
                        sender.sendMessage(ChatColor.YELLOW + "Reloading...");
                        try {
                            SystemChat.getInstance().reloadConfig();
                            sender.sendMessage(ChatColor.YELLOW + "Reloaded.");
                        } catch (Exception e) {
                            String errorMessage;
                            if (e instanceof InvalidConfigurationException) {
                                errorMessage = "Configuration is not a valid YAML document:";
                                sender.sendMessage(ChatColor.RED + errorMessage + "\n" + e.getLocalizedMessage());
                            } else {
                                errorMessage = "Error while reloading configuration:";
                                sender.sendMessage(ChatColor.RED + errorMessage + "\n" + e.getLocalizedMessage());
                            }
                            SystemChat.getInstance().getLogger().log(Level.SEVERE, errorMessage, e);
                        }
                    } else {
                        sender.sendMessage(ChatColor.RED + "No permission.");
                    }
                    break;
                default:
                    sender.sendMessage(aboutMessage);
                    break;
            }
        } else {
            sender.sendMessage(aboutMessage);
        }
        return true;
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        return args.length > 0 ? BASE_COMPLETIONS.stream().filter(s -> s.startsWith(args[0].toLowerCase())).toList() : BASE_COMPLETIONS;
    }

}
