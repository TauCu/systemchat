plugins {
    id("java")
    id("io.papermc.paperweight.userdev") version "2.0.0-SNAPSHOT"
    id("net.minecrell.plugin-yml.bukkit") version "0.6.0"
    id("com.gradleup.shadow") version "8.3.4"
}

group = "me.taucu"
version = "1.10.0"

repositories {
    mavenCentral()
    mavenLocal()
    maven { url = uri("https://papermc.io/repo/repository/maven-public/") }
    maven { url = uri("https://papermc.io/repo/repository/maven-snapshots/") }
    maven { url = uri("https://hub.spigotmc.org/nexus/content/groups/public/") }
}

dependencies {
    paperweight.paperDevBundle("1.21.4-R0.1-SNAPSHOT")
    //paperweight.foliaDevBundle("1.20.5-R0.1-SNAPSHOT")

    // bstats
    implementation("org.bstats:bstats-bukkit:3.0.2")
}

java {
    // Configure the java toolchain. This allows gradle to auto-provision JDK 21 on systems that only have JDK 8 installed for example.
    toolchain.languageVersion.set(JavaLanguageVersion.of(21))
}

tasks {
    // Configure reobfJar to run when invoking the build task
    assemble {
        dependsOn(reobfJar)
    }

    shadowJar {
        relocate("org.bstats", "me.taucu.systemchat.lib.bstats")
        archiveClassifier = ""
        mustRunAfter(jar)
    }

    compileJava {
        // Set the release flag. This configures what version bytecode the compiler will emit, as well as what JDK APIs are usable.
        // See https://openjdk.java.net/jeps/247 for more information.
        options.release.set(21)
    }

}

bukkit {
    main = "me.taucu.systemchat.SystemChat"
    name = "SystemChat"

    author = "TauCubed <nullvoxel@gmail.com>"
    description = "Prevents chat reporting, blocks the \"insecure server\" popup, prevents insecure chat message warnings and bypasses \"only show secure chat\""
    version = project.version.toString()

    commands {
        register("systemchat") {
            description = "SystemChat's main command."
            usage = "/systemchat reload"
        }
    }

    load = net.minecrell.pluginyml.bukkit.BukkitPluginDescription.PluginLoadOrder.STARTUP

    apiVersion = "1.21"
    foliaSupported = true
}