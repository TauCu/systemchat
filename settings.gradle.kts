rootProject.name = "SystemChat"

pluginManagement {
    repositories {
        //mavenLocal()
        gradlePluginPortal()
        maven { url = uri("https://papermc.io/repo/repository/maven-public/") }
    }
}
